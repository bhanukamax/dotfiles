# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/$USER/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
#ZSH_THEME="robbyrussell"
#ZSH_THEME="amuse"
#ZSH_THEME="xiong-chiamiov"
#ZSH_THEME="gnzh"
ZSH_THEME="tjkirch"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git vi-mode)

source $ZSH/oh-my-zsh.sh

export DOT_FILES_DIR=$(dirname $(readlink -f ~/.zshrc))


# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language envliaironment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
 if [[ -n $SSH_CONNECTION ]]; then
   export EDITOR='vim'
 else
   export EDITOR='nvim'
 fi

 export VISUAL='nvim'

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
alias gs="git status"
alias gap="git add -p"
alias al="alias | grep "
alias cm="git checkout master"
alias cb="git checkout -"
alias c="git checkout "
alias gw="git worktree "
alias v="nvim"
alias dv="VIMRUNTIME=runtime ~/foss/neovim/build/bin/nvim" # for nvim debugging
alias vd="neovide"
#alias v="VIMRUNTIME=runtime ~/foss/neovim/build/bin/nvim"
alias doc="su - ${USER}"

alias ez="nvim $DOT_FILES_DIR/.zshrc"
alias ze="nvim $DOT_FILES_DIR/.zshrc"
alias sz="source $DOT_FILES_DIR/.zshrc"
alias zs="source $DOT_FILES_DIR/.zshrc"

alias etmux="nvim $DOT_FILES_DIR/.tmux.conf"

alias amux="tmux a"

alias eal="nvim $DOT_FILES_DIR/.config/alacritty/alacritty.yml"

alias eaw="nvim $DOT_FILES_DIR/.config/awesome/rc.lua"
alias aw="cd $DOT_FILES_DIR/.config/awesome/"



alias i="npm i"
alias n="npm"
alias nr="npm run"
alias start="npm run start"
alias nlink="npm link"
alias nunlink="npm unlink"
alias copy="xsel -ib"
alias gl="glog"
alias pull="git pull"
alias fetch="git fetch"
alias rebase="git rebase"
alias stash="git stash"
alias stpop="git stash pop"
alias st="git stash"
alias timer="python /home/$USER/git-apps/auto-timer-fixed/autotimer.py"

alias ei3="nvim $DOT_FILES_DIR/.i3/config"
alias esway="nvim $DOT_FILES_DIR/.config/sway/config"
alias epoly="nvim $DOT_FILES_DIR/.config/polybar/config"
alias eq="nvim $DOT_FILES_DIR/.config/qtile/config.py"


alias resetaudio="rm -r $DOT_FILES_DIR/.config/pulse; pulseaudio -k"
alias restartpoly="killall -p polybar && polybar example && echo 'done'"

alias dotfiles='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
alias dt='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
alias dot='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
alias ds='dotfiles status'
alias da="dotfiles add "
alias dap="dotfiles add -p"
alias dc="dotfiles commit"
alias dch="dotfiles checkout"
alias dd="dotfiles diff"
alias c="git checkout"
alias lg="lazygit"
alias gu="gitui"
alias s="serve -s"
alias harpoon="cd ~/foss/bmax/harpoon"

# Tmuxinator
alias mux=tmuxinator
alias tmuxa="tmux a"
alias startweb="mux web"
alias sweb="mux web"
alias cweb="v ~/.config/tmuxinator/web.yml"
alias stopweb="mux stop web"
alias stweb="mux stop web"
alias mrun="mux run"
alias sdots="mux dots"
alias stdots="mux stop dots"

export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm


# This enables docker-compose without sudo
export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools

source $HOME/.local-config.sh


export BROWSER=/usr/bin/google-chrome-stable

#export PROMPT_COMMAND="pwd > /tmp/whereami"
#precmd() { eval "$PROMPT_COMMAND" }

#neofetch
#tmux || true
#cd $MYPATH


# Emacs
alias ec='emacsclient -a "" -c'
alias et='emacsclient -a "" -t'


#alias lsls="ls"
alias l="lsd -lah"
#alias ls="lsd"




#figlet -f Bloody "Bmax" | lolcat
#cowsay -f stegosaurus "Good times!!" | lolcat




open_man_in_vim () {
       echo 'hello, world $1'
       echo $1
       man $1 | nvim - -R
}

alias vp='nvim --cmd "set rtp+=$(pwd)"' # add current directory to runtime for plugin development
alias manvim="open_man_in_vim"
alias pvim="nvim - -R"
alias bnvim="/home/bm/foss/neovim/build/bin/nvim"
alias vpb='/home/bm/foss/neovim/build/bin/nvim --cmd "set rtp+=$(pwd)"' # add current directory to runtime for plugin development

# opam configuration
test -r /home/bm/.opam/opam-init/init.zsh && . /home/bm/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true

alias hcaml=/home/bm/test/ocaml-stuff/hcaml-compiler/src/compiler.js

PATH=/usr/class/bin:$PATH
PATH=/home/$USER/.deno/bin:$PATH

# Atom stuff
#export ATOM_DEV_RESOURCE_PATH=/home/$USER/foss/editors/atom
open_repl() {
  cd "/home/$USER/test/node-stuff/"
  v "./repl.js"
}
alias repl=open_repl

alias music="mpv https://www.youtube.com/watch\?v\=c8ijHgTvv9I --no-video"


#source '/home/bm/lib/azure-cli/az.completion'
alias l="ls -CF"

export PATH=$PATH:/usr/local/go/bin
alias mp="mpv --no-video "
alias track1="mp https://www.youtube.com/watch\?v\=DWaoba9NqEA"
alias lit="~/code/tui-stuff/py-light-app/main.py"

# Journals
alias vj="$EDITOR ~/Dropbox/journals/Vim.md"
alias rj="$EDITOR ~/Dropbox/journals/Rust.md"
alias wj="$EDITOR ~/Dropbox/journals/workflow.md"
alias cj="$EDITOR ~/Dropbox/journals/career.md"
alias lj="$EDITOR ~/Dropbox/journals/learning.md"
alias arj="$EDITOR ~/Dropbox/journals/arduino.md"
alias jj="$EDITOR ~/Dropbox/journals/job.md"
alias hj="$EDITOR ~/Dropbox/journals/hack.md"

# Rust playground
alias rp="cd ~/code/rust-stuff/playground"


alias dots="cd $DOT_FILES_DIR"
alias asdf="setxkbmap -v us dvorak"
alias asdff="setxkbmap -v us real-prog-dvorak"
alias aoeu="setxkbmap -v us"
alias keyboard="sh ~/.xprofile"
alias i3r="i3-msg restart"
alias wallpaper="nitrogen --restore"


# Adding neovim local builds to path
if [ -d "$HOME/foss/neovim/build/bin/" ] ; then
  PATH="$PATH:$HOME/foss/neovim/build/bin/"
fi


alias dvorak="eog ~/Pictures/keyboard/dvorkeys.png"


# Caps lock remaps
#setxkbmap -option ctrl_modifier:super_modifier
#setxkbmap -option caps:ctrl_modifier
#xcape -e 'Caps_Lock=Escape'
#xcape -e 'Control_L=Escape'
#setxkbmap -option caps:super_l
#
#



#alias yt_screenkeys="screenkey -s large -p bottom --geometry 510x200+600+720 --scr 1";
alias yt_screenkeys="screenkey -s large -p bottom --geometry 510x200+600+720 --scr 1"
alias untar="tar -xzf"


function bare() {
    # TODO: this needs to convert non bare repo to bare repo
    # steps from stack overflow
    # cd repo
    # mv .git ../repo.git # renaming just for clarity
    # cd ..
    # rm -fr repo
    # cd repo.git
    # git config --bool core.bare true

    dirname=${PWD##*/}
  if git rev-parse --is-inside-work-tree; then
      echo "inside a git folder"
  else
      echo "not a git folder"
  fi
  echo "directory name " . $dirname

}


# TODO: write  a function to convert from bare to nomal repo
function unbare() {
#    https://stackoverflow.com/questions/10637378/how-do-i-convert-a-bare-git-repository-into-a-normal-one-in-place

}


alias lsp="cd ~/code/try-lsp/ts"

function CmakeClean() {
    rm -rf CMakeFiles
    rm -rf CMakeCache.txt
    rm -rf cmake_install.cmake
}


alias dockerstopall="docker ps --format '{{.ID}}' | xargs docker stop"
