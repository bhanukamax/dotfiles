# How to manage dot files

https://www.atlassian.com/git/tutorials/dotfiles


## Creating a new dotfiles repo

```bash

git init --bare $HOME/dotfiles
alias dots='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
dots config --local status.showUntrackedFiles no
echo "alias dot='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'" >> $HOME/.zshrc

```

# Installing dotfiles onto a new system


** add follwoing to bashrc or zshrc **

```
alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'


alias dots='/usr/bin/git --git-dir=$HOME/.dots/ --work-tree=$HOME'

echo ".dots" >> .gitignore

git clone --bare git@github.com:Bhanukamax/dotfiles.git $HOME/.dots
```





