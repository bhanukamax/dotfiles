
" Remap for do codeAction of selected region
"function! s:cocActionsOpenFromSelected(type) abort
  "execute 'CocCommand actions.open ' . a:type
"endfunction
"xmap  <leader>a :<C-u>execute 'CocCommand actions.open ' . visualmode()<CR>
"nmap <leader>a :<C-u>set operatorfunc=<SID>cocActionsOpenFromSelected<CR>g@

" Diagnostic
fun! UseCoc()
nnoremap <leader>dp :call CocAction('diagnosticPrevious')<CR>
nnoremap <leader>dn :call CocAction('diagnosticNext')<CR>
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

nmap <leader>rn <Plug>(coc-rename)
nmap <leader>bf :CocCommand prettier.formatFile<CR>

"map <leader>ac <Plug>(coc-codeaction-selected)
" Code Actions to move parts to a new fie
vmap <leader>ac  <Plug>(coc-codeaction-selected)
xmap <leader>ac  <Plug>(coc-codeaction-selected)
nmap <leader>ac  <Plug>(coc-codeaction-selected)
map <leader><leader>q <Plug>(coc-codeaction)

nmap <silent> <leader>rs <Plug>(coc-range-select)
xmap <silent> <leader>rs <Plug>(coc-range-select)

nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gs :CocCommand tsserver.organizeImports<CR>
"nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

nnoremap <leader>cr :CocRestart<CR>


nnoremap <leader>cd :CocDiagnostics<CR>

" Auto import
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm() : "\<C-g>u\<CR>"

endfun
if g:bmax_use_lsp == 0
    call UseCoc()
endif




augroup cpp_mapping
    autocmd!
    autocmd filetype cpp call CppMapping()
    autocmd filetype c call CppMapping()
augroup END

function! CppMapping()
    nmap <leader>po :CocCommand clangd.switchSourceHeader<CR>
endfunction


command! -nargs=? Fold :call     CocAction('fold', <f-args>)

nnoremap zo :Fold<CR>


"set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}
