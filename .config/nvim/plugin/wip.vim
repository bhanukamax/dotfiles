
function! Cheat(query)
  let query = 'cheat.sh/' . a:query
  execute 'split | term curl ' . query
  execute 'resize ' . string(&lines/3)
endfunction

command! -nargs=1 CheatSh call Cheat(<q-args>)
"nnoremap <leader>hc :CheatSh <C-R>=&filetype<CR>/


autocmd FileType markdown nmap <buffer><silent> <leader>p :call mdip#MarkdownClipboardImage()<CR>

function! BmaxVimWikiPreviewStop()
        augroup vimviki_mapping
            autocmd!
        augroup END
endfunction

function! BmaxVimWikiPreview()
    :MarkdownPreview
    if &ft == 'vimwiki'
        augroup vimviki_mapping
            autocmd!
            autocmd BufEnter *.md :MarkdownPreview
        augroup END
    else
        augroup vimviki_mapping
            autocmd!
        augroup END
    endif
endfunction


" TODO: this code can list colorscheme
" need to write a color scheme switcher either with fzf or telescope
function! Bmax_find_theme_names() " {{{1
  " Get a sorted list with the available color schemes.
  let matches = {}
  "let exclude_list = xolox#misc#option#get('colorscheme_switcher_exclude', [])
  "let exclude_builtins = xolox#misc#option#get('colorscheme_switcher_exclude_builtins', 0)
  for fname in split(globpath(&runtimepath, 'colors/*.vim'), '\n')
    let name = fnamemodify(fname, ':t:r')
    " Ignore names in the exclude list.
    "if index(exclude_list, name) == -1
      " Ignore color schemes bundled with Vim?
      "if !(exclude_builtins && xolox#misc#path#starts_with(fname, $VIMRUNTIME))
        let matches[name] = 1
        echo  name
      "endif
    "endif
  endfor
  return sort(keys(matches), 1)
endfunction
