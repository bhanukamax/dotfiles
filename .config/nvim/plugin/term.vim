
" Terminal Keys
:tnoremap <Esc> <C-\><C-n>
:tnoremap <C-[> <C-\><C-n>
":tnoremap <C-c> <C-\><C-n>
:tnoremap <C-w> <C-\><C-N><C-w>
:tnoremap <A-h> <C-\><C-N><C-w>h
:tnoremap <A-j> <C-\><C-N><C-w>j
:tnoremap <A-k> <C-\><C-N><C-w>k
:tnoremap <A-l> <C-\><C-N><C-w>l
