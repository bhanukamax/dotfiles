set exrc
set foldcolumn=3
set noswapfile
"set relativenumber
set hidden
set noerrorbells
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
"set nu
set nowrap
set smartcase
set ignorecase
set nobackup
set mouse=nicr " Disallow visual selection with mouse
set guicursor=
"set nohlsearch
set undodir=~/.vim/undodir
set undofile
set incsearch
set termguicolors
set scrolloff=8
set noshowmode
set completeopt=menuone,noinsert,noselect
"set mouse=a " Allows visual selection with mouse
set colorcolumn=100
set splitright
set splitbelow
set encoding=UTF-8
set inccommand=split
set list
set nu
set relativenumber
set listchars+=tab:>-,eol:↲,nbsp:%,trail:_
set cursorline

"set foldmethod=expr
"set foldexpr=nvim_treesitter#foldexpr()
"set foldnestmax=3
