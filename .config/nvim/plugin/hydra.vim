let s:example_hydra =
            \ {
            \   'name':        'example',
            \   'title':       'Harpoon',
            \   'show':        'popup',
            \   'exit_key':    "q",
            \   'feed_key':    v:true,
            \   'foreign_key': v:true,
            \   'keymap-hide': v:true,
            \   'keymap': [
            \     {
            \       'name': 'Move to',
            \       'keys': [
            \         ['j', "lua require'bmax.harpoon'.nav_file(1)", 'harpoon 1'],
            \         ['k', "lua require'bmax.harpoon'.nav_file(2)", 'harpoon 2'],
            \         ['l', "lua require'bmax.harpoon'.nav_file(3)", 'harpoon 3'],
            \         [';', "lua require'bmax.harpoon'.nav_file(4)", 'harpoon 4'],
            \         ['u', "lua require'bmax.harpoon'.nav_file(5)", 'harpoon 5'],
            \         ['i', "lua require'bmax.harpoon'.nav_file(6)", 'harpoon 6'],
            \         ['o', "lua require'bmax.harpoon'.nav_file(7)", 'harpoon 7'],
            \         ['p', "lua require'bmax.harpoon'.nav_file(8)", 'harpoon 8'],
            \         ['t', "call Harpoon_GotoTerminal(1)", 'Term 1'],
            \         ['r', "call Harpoon_GotoTerminal(2)", 'Term 2'],
            \       ]
            \     },
            \     {
            \       'name': 'Resize',
            \       'keys': [
            \         [',', "wincmd <", '>>'],
            \         ['.', "wincmd >", '<<'],
            \         ['-', "wincmd -", '-'],
            \         ['=', "wincmd +", '+'],
            \       ]
            \     },
            \     {
            \       'name': 'Buffers',
            \       'keys': [
            \         ['m', "lua require'bmax.harpoon'.nav_prev()", 'harp prev'],
            \         ['n', "lua require'bmax.harpoon'.nav_next()", 'harp next'],
            \       ]
            \     },
            \   ]
            \ }

silent call hydra#hydras#register(s:example_hydra)

nnoremap Q :Hydra example<CR>
