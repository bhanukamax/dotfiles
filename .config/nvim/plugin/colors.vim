
let g:gruvbox_invert_selection='1'
let g:gruvbox_contrast_dark = 'soft'
"let g:material_theme_style = 'ocean'
let g:gruvbox_italicize_comments = "1"
let g:gruvbox_contrast_light = 'soft'

"colorscheme embark
"colorscheme gruvbox
nnoremap <leader>vc :colorscheme
"colorscheme strawimodo


"colorscheme onehalfdark


"let g:lightline = {
      "\ 'colorscheme': 'onedark',
      "\ }


"hi Normal guibg=NONE ctermbg=#1b2b34
"colorscheme onehalflight
"colorscheme onehalfdark
"let g:airline_theme='onehalfdark'
"if exists('+termguicolors')
  "let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  "let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  "set termguicolors
"endif
"lua require('colorbuddy').colorscheme('gruvbuddy')

"colorscheme onedark

"colorscheme gruvbuddy
"
"
" THEME
" Ayu
"set termguicolors     " enable true colors support
"let ayucolor="light"  " for light version of theme
":call ColorAyu()
"
"

function! s:HL(group, fg, bg, attr)
    let l:attr = a:attr
    if !g:deepspace_italics && l:attr ==# 'italic'
        let l:attr = 'none'
    endif

    if !empty(a:fg)
        exec 'hi ' . a:group . ' guifg=' . a:fg
    endif
    if !empty(a:bg)
        exec 'hi ' . a:group . ' guibg=' . a:bg
    endif
    if !empty(a:attr)
        exec 'hi ' . a:group . ' gui=' . l:attr . ' cterm=' . l:attr
    endif
endfun

fun! ColorGruvbox()
    let s:gray6     = "#9faebf"
    let s:gray5     = '#9aa7bd'
    let s:gray3     = '#323c4d'
    let s:gray2     = '#232936'
    let s:gray1     = '#1b202a'
    let s:gray0     = '#181b1f'
    let s:purple    = '#423f5d'

    colorscheme gruvbox
    let s:bg = s:gray2
    let s:shadow = s:gray0

    call s:HL('NonText',     s:shadow,  s:shadow ,  'none')
    call s:HL('FoldColumn',  '',        s:shadow,   '')

    call s:HL('LineNr',      '',        s:shadow,   '')


    hi link GitGutterAdd    SignifySignAdd
    hi link GitGutterChange SignifySignChange
    hi link GitGutterDelete SignifySignDelete
    call s:HL('GitGutterChangeDelete',    "",   s:shadow,        "")
    call s:HL('GitGutterAdd',             "",   s:shadow,        "")
    call s:HL('GitGutterChangeDeleteLine',          "red",   s:shadow,        "")
    call s:HL('GitGutterChange',          "red",   s:shadow,        "")
    call s:HL('GitGutterDelete',          "red",   s:shadow,        "")
    call s:HL('Normal',                  s:gray6,    s:bg,    'none')
endfun

function! ReturnHighlightTerm(group, term)
   " Store output of group to variable
   let output = execute('hi ' . a:group)

   " Find the term we're looking for
   return matchstr(output, a:term.'=\zs\S*')
endfunction

fun! ColorDark(A)
    let s:gray6     = "#9faebf"
    let s:gray5     = '#9aa7bd'
    let s:gray3     = '#323c4d'
    let s:gray2     = '#232936'
    let s:gray1     = '#1b202a'
    let s:gray0     = '#181b1f'
    let s:purple    = '#423f5d'


    execute("colorscheme " . a:A)
    "colorscheme a:A
    let s:bg = s:gray2
    "let s:shadow = s:gray0
    let s:shadow = s:gray0
    "let s:shadow = "#1b1f1e"
    "
    "let s:shadow = "#1b1f1e"

    "
    let s:normalbg = ReturnHighlightTerm("Normal", "guibg")
    echo s:normalbg
    "
    "

    call s:HL('NonText',     s:shadow,  s:shadow ,  'none')
    call s:HL('FoldColumn',  '',        s:shadow,   '')

    call s:HL('LineNr',      '',        s:shadow,   '')


    hi link GitGutterAdd    SignifySignAdd
    hi link GitGutterChange SignifySignChange
    hi link GitGutterDelete SignifySignDelete
    call s:HL('GitGutterChangeDelete',    "",   s:shadow,        "")
    call s:HL('GitGutterAdd',             "",   s:shadow,        "")
    call s:HL('GitGutterChangeDeleteLine',          "red",   s:shadow,        "")
    call s:HL('GitGutterChange',          "red",   s:shadow,        "")
    call s:HL('GitGutterDelete',          "red",   s:shadow,        "")
    call s:HL('Normal',                  s:gray6,    s:bg,    'none')

endfun


fun! ColorLight(name)
    let s:gray6     = "#9faebf"
    let s:gray5     = '#9aa7bd'
    let s:gray3     = '#323c4d'
    let s:gray2     = '#232936'
    let s:gray1     = '#1b202a'
    let s:gray0     = '#181b1f'
    let s:purple    = '#423f5d'

    "execute("colorscheme " . a:name)
    execute a:name
    let s:bg = s:gray3
    let s:shadow = s:gray2

    call s:HL('NonText',     s:shadow,  s:shadow ,  'none')
    call s:HL('FoldColumn',  '',        s:shadow,   '')

    call s:HL('LineNr',      '',        s:shadow,   '')


    hi link GitGutterAdd    SignifySignAdd
    hi link GitGutterChange SignifySignChange
    hi link GitGutterDelete SignifySignDelete
    call s:HL('GitGutterChangeDelete',    "",   s:shadow,        "")
    call s:HL('GitGutterAdd',             "",   s:shadow,        "")
    call s:HL('GitGutterChangeDeleteLine',          "red",   s:shadow,        "")
    call s:HL('GitGutterChange',          "red",   s:shadow,        "")
    call s:HL('GitGutterDelete',          "red",   s:shadow,        "")
    "call s:HL('Normal',                  s:gray6,    s:bg,    'none')
    "call s:HL('Normal',                  s:gray6,    s:bg,    'none')
endfun


set background=dark
"call ColorDark("deep-space")
"call ColorLight("gruvbox")
"call ColorDark(deep-space)

fun! ColorAyu()
    let g:ayucolor="mirage" " for mirage version of theme
    ""let ayucolor="dark"   " for dark version of theme
    call ColorLight("ayu")
    let ayucolor="mirage" " for mirage version of theme


     "One Half
    "set t_Co=256
    hi NonText                   guifg=#3b465c guibg=#272d38  gui=none
endfun
"call ColorAyu()
"
"
"
fun! ColorListChars(name)
    "let command = "hi NonText   guifg=" . a:name . "  guibg=" . a:name . "  gui=none"
    call s:HL('NonText',    a:name,   a:name,        "none")
    "execute(command)
endfun

command! -nargs=1 SetListCharColor call ColorListChars(<args>)

command! -nargs=1 -complete=color Bmaxcolor call ColorDark(<q-args>)


"Bmaxcolor deep-space









lua << EOF
EOF
"local saga = require 'lspsaga'
"saga.init_lsp_saga()

fun! Lsp_bindings()
    inoremap <silent><expr> <C-Space> compe#complete()
    inoremap <silent><expr> <CR>      compe#confirm('<CR>')
    nmap <leader>ac :lua vim.lsp.buf.code_action()<CR>
    vmap <leader>ac :lua vim.lsp.buf.code_action()<CR>
    "nnoremap <silent><leader>ac <cmd>lua require('lspsaga.codeaction').code_action()<CR>
    "vnoremap <silent><leader>ac :<C-U>lua require('lspsaga.codeaction').range_code_action()<CR>
    "vnoremap <silent><leader>av :<C-U>lua require('lspsaga.codeaction').what_max()<CR>

    "nnoremap <silent>rn :Lspsaga rename<CR>
    "nnoremap <silent>gd :Lspsaga lsp_finder<CR>
    nnoremap <silent>gd :lua vim.lsp.buf.definition()<CR>
    nnoremap <silent>gh :lua vim.lsp.buf.hover()<CR>
    nnoremap <silent>rn :lua vim.lsp.buf.rename()<CR>
    nnoremap <leader>cr :LspRestart<CR>
    nnoremap <s-k> :lua vim.lsp.buf.hover()<cr>
    "nnoremap <silent>rn :Lspsaga rename<CR>
endfun

":lua vim.cmd('Bmaxcolor deep-space')
":lua vim.cmd('colorscheme deep-space');
:lua vim.cmd('colorscheme gruvbox');
":lua vim.cmd('colorscheme oceanic_material')

hi normal guibg=none ctermbg=none
hi Todo guibg=none ctermbg=none
hi LineNr guibg=none ctermbg=none


"Bmaxcolor deep-space


if g:bmax_use_lsp == 1
    :lua require('bmax.lsp').setup()
    call Lsp_bindings()
    echo "using lsp"
endif

