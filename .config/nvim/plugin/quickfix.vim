nnoremap <leader>co :copen<cr>
nnoremap <c-j> :cprevious<cr>
nnoremap <c-k> :cnext<cr>

nnoremap <leader>lo :lopen<cr>
nnoremap <leader>j :lprev<cr>
nnoremap <leader>k :lnext<cr>

" TERMINAL
nnoremap <leader>ta :call OpenTerm()<CR>
nnoremap <leader>tf :call OpenFloatingTerm()<CR>
