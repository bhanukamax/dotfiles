" Puts the cursor on the last open file
fun! OpenNetrw()
    let s:file_name = expand('%:t')
    :Ex
    :call search(s:file_name)
endfun

"let g:netrw_list_hide= "\.*\.o$,TodoQt,.qmake.stash"

"nnoremap - :lua require("bmax.netrw").open_netrw()<CR>
nnoremap - :call OpenNetrw()<CR>
nnoremap <leader>ee :call OpenNetrw()<CR>
"nnoremap _ :vs<CR> :lua require("bmax.netrw").open_netrw()<CR>


"let g:netrw_browse_split = 2
let g:netrw_banner = 0
"let g:netrw_winsize = 25
"let g:netrw_localrmdir='rm -r'
"let g:netrw_preview = 1
"
augroup netrw_mapping
    autocmd!
    autocmd filetype netrw call NetrwMapping()
augroup END

function! NetrwMapping()
    nmap <buffer> h -
    nmap <buffer> l <CR>
endfunction

" for setting relativenumber
let g:netrw_bufsettings="noma nomod rnu nobl nowrap ro"

map <leader>ev :Vifm<CR>
map <leader>ev :Vifm<CR>
"nnoremap - :Ex<CR>
command! -bang -nargs=* Boop lua require'bmax.netrw'.Boop(<f-args>)
