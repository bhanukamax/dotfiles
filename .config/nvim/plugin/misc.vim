" ALl the stuff I'm yet to move to a proper place :)
" Undotree
nnoremap <leader>uu :UndotreeToggle<CR>

" Mfaximize
nnoremap <c-w><c-m> :MaximizerToggle<CR>
nnoremap <leader>bo :BufOnly<CR>

" Spellings
"nnoremap <c-j> <Esc>z=`1<CR><CR>

" Git blame
let g:blamer_enabled = 1
let g:blamer_delay = 500

" Line numbers
map <leader>N :set nu! relativenumber! <CR>

" VIMRC
map <leader>ve :e $DOT_FILES_DIR/.config/nvim/init.vim<CR>
map <leader>sv :so %<CR>
map <leader>sl :luafile %<CR>

"set completeopt=longest,menuone
function! OpenCompletion()
    if !pumvisible() && ((v:char >= 'a' && v:char <= 'z') || (v:char >= 'A' && v:char <= 'Z'))
        call feedkeys("\<C-x>\<C-o>", "n")
    endif
endfunction

autocmd InsertCharPre *.ml call OpenCompletion()

" emmet
imap <expr> <tab> emmet#expandAbbrIntelligent("\<tab>")
let g:user_emmet_settings = {
\  'javascript' : {
\      'extends' : 'jsx',
\  },
\}

" Save
inoremap <c-s> <Esc>:w<CR>l
nnoremap <c-s> :w<CR>


" Silver search
nmap <leader>ag :Ag!<space>

" Folds
nmap <leader>fa :AnyFoldActivate<CR>

let g:VimTodoListsUndoneItem = '- [ ]'
let g:VimTodoListsDoneItem = '- [X]'
" Editing
" "Delete next letter while editing by pressing Ctrl+l
inoremap <c-l> <Esc>lxi



" Cyclist
"call cyclist#set_eol('default', '↵')
"call cyclist#set_space('default', '𝁡')
"
"
"


fun! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun

augroup THE_BMAX
    autocmd!
    autocmd BufWritePre * :call TrimWhitespace()
    "autocmd BufEnter *.{js,jsx,ts,tsx} :syntax sync fromstart
    "autocmd BufLeave *.{js,jsx,ts,tsx} :syntax sync clear
    "autocmd BufEnter *.{rfc} :set readonly

    au TextYankPost * silent! lua vim.highlight.on_yank()
    " FORMATTERS
    au FileType javascript setlocal formatprg=prettier
    au FileType javascript.jsx setlocal formatprg=prettier
    au FileType json setlocal formatprg=prettier
    au FileType typescriptreact setlocal formatprg=prettier

    "au FileType typescript setlocal formatprg=prettier\ --parser\ typescript
    au FileType typescript setlocal formatprg=prettier
    au FileType html setlocal formatprg=js-beautify\ --type\ html
    au FileType scss setlocal formatprg=prettier\ --parser\ css
    au FileType css setlocal formatprg=prettier\ --parser\ css
augroup END


"nnoremap <C-Left> :call AfPPAlternate()<CR>

" This allows for the retrieval of 2 files ago
"nnoremap <C-Down> :call AfPPAlternatePluthPluth()<CR>

set encoding=UTF-8
  let g:webdevicons_enable_vimfiler = 1

if has("persistent_undo")
   let target_path = expand('~/.undodir')

    " create the directory and any parent directories
    " if the location does not exist.
    if !isdirectory(target_path)
        call mkdir(target_path, "p", 0700)
    endif

    let undodir=target_path
    set undofile
endif


set termguicolors
:lua require'colorizer'.setup()


iabbrev cosnt const


