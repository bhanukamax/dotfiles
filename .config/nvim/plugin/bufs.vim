
" Buffers
"map <Tab> :BufMRUNext<CR>
"map <S-Tab> :BufMRUPrev<CR>
"map <Tab> :bn<CR>
"map <S-Tab> :bp<CR>
command! Bd bp | sp | bn | bd
inoremap jk <ESC>

map <leader>\ :vsp<CR>
map <leader>- :sp<CR>

map <leader>bl :Buffers<CR>

"map \ :HopWord<CR>
"map <leader>\ :lua print ("CTRL-W V")<CR>

vmap $ g_

"nnoremap <A-j> :m .+1<CR>==
"nnoremap <A-k> :m .-2<CR>==
"inoremap <A-j> <Esc>:m .+1<CR>==gi
"inoremap <A-k> <Esc>:m .-2<CR>==gi
"vnoremap <A-j> :m '>+1<CR>gv=gv
"vnoremap <A-k> :m '<-2<CR>gv=gv

map <leader>fs :set foldmethod=syntax<cr>zR
map <leader>fm :set foldmethod =manual <cr>zR


" break line
"nnoremap <cr> i<cr> <Esc>^
imap <c-j> <Esc>l~hi

