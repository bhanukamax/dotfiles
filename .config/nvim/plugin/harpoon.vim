fun! OpenBottomTerm()
    :sp
    :wincmd J
    :lua current_win = vim.api.nvim_get_current_win()
    :lua vim.api.nvim_win_set_height(current_win, 10)
    :lua require("harpoon.term").gotoTerminal(1)
    :set nobuflisted
endfun

fun! OpenRightTerm()
    :vsp
    :wincmd L
    :lua current_win = vim.api.nvim_get_current_win()
    :lua vim.api.nvim_win_set_width(current_win, 50)
    :lua require("harpoon.term").gotoTerminal(1)
    :set nobuflisted
endfun


" for testing reorder auto save on harpoon buf leave, but still don't work
augroup BMAX_HARPOON
    autocmd!
    autocmd BufLeave harpoon :lua require('harpoon.ui').on_menu_save()
    autocmd BufLeave harpoon :lua require('harpoon.ui').save_changes()
augroup END


"nnoremap <leader><leader>t :call Harpoon_GotoTerminal(1)<CR> :set nobuflisted<CR>
nnoremap <leader><leader>t :call OpenBottomTerm()<CR>
nnoremap <leader><leader>v :call OpenRightTerm()<CR>
"nnoremap <leader><leader>t :call OpenBottomTerm()<CR> :set nobuflisted<CR>
nnoremap <leader>tt :lua require("harpoon.term").gotoTerminal(1)<CR> :set nobuflisted<CR>
nnoremap <leader>tj :lua require("harpoon.term").gotoTerminal(1)<CR> :set nobuflisted<CR>
nnoremap <leader>tk :lua require("harpoon.term").gotoTerminal(2)<CR> :set nobuflisted<CR>
nnoremap <leader>t2 :lua require("harpoon.term").gotoTerminal(2)<CR> :set nobuflisted<CR>



nnoremap <silent> <leader>ts :split<CR><C-w><C-j>:resize 10<CR>:lua require("harpoon.term").gotoTerminal(1)<CR>a



nnoremap <leader>ha :lua require("harpoon.mark").add_file()<CR>
nnoremap <leader><leader>h :lua require("harpoon.ui").toggle_quick_menu()<CR>
nnoremap <leader>hl :lua require("harpoon.ui").toggle_quick_menu()<cr>
nnoremap <leader>h; :lua require("harpoon.ui").toggle_quick_menu()<cr>

nnoremap <silent><c-h> :lua require("bmax.harpoon").nav_file(1)<CR>
nnoremap <silent><c-n> :lua require("bmax.harpoon").nav_file(2)<CR>
nnoremap <silent><c-b> :lua require("bmax.harpoon").nav_file(3)<CR>
nnoremap <silent><c-t> :lua require("harpoon.term").gotoTerminal(1)<CR> :set nobuflisted<CR>
nnoremap <silent><c-q> :lua require("harpoon.term").gotoTerminal(2)<CR> :set nobuflisted<CR>

"nnoremap <silent><c-m> :lua require("bmax.harpoon").nav_file(4)<CR>

nnoremap <leader>hr :lua require("harpoon.mark").rm_file()<CR>
nnoremap <leader><C-r> :lua require("harpoon.mark").shorten_list()<CR>
nnoremap <leader>hg :lua require("harpoon.mark").promote()<CR>
nnoremap <leader>hu :lua require("harpoon.mark").promote()<CR>
"nnoremap <leader>tu :lua require("harpoon.term").gotoTerminal(1)<CR>
"nnoremap <leader>te :lua require("harpoon.term").gotoTerminal(2)<CR>
"nnoremap <leader>cu :lua require("harpoon.term").sendCommand(1, 1)<CR>
"nnoremap <leader>ce :lua require("harpoon.term").sendCommand(1, 2)<CR>


nnoremap <silent> <C-Left> :lua require("harpoon.ui").nav_prev()<CR>:echo bufname()<CR>
nnoremap <silent> <C-Down> :lua require("harpoon.ui").nav_next()<CR>:echo bufname()<CR>
nnoremap <leader>hc :lua require("harpoon.mark").clear_all()<CR>


highlight HarpoonWindow ctermbg=yellow ctermfg=red
highlight HarpoonBorder ctermbg=yellow ctermfg=red

augroup harpoon_menu_mapping
    autocmd!
    autocmd filetype harpoon call HarpoonMenuMapping()
augroup END

function! HarpoonMenuMapping()
    nmap <buffer> <c-k> :m -2<CR>
    nmap <buffer> <c-j> :m +1<CR>
    nmap <buffer> <c-q> :lua require("harpoon.mark").to_quickfix_list()<CR>
    nmap <buffer> q :wq<CR>
endfunction

