
" NERD Tree
"let g:netrw_liststyle = 3
"let g:netrw_banner = 0
"map <jeader>nt :NERDTreeToggle<CR>
" NVIM Tree
map <leader>n :NvimTreeToggle<CR>:set nu<CR>
"noremap <leader>m :NERDTreeFind<CR>
noremap <leader>m :NvimTreeFindFile<CR>:set rnu<CR>

let g:nvim_tree_disable_netrw = 0
let g:nvim_tree_hijack_netrw = 0
let g:nvim_tree_width_allow_resize = 1

"map \ :RnvimrToggle<CR>
map <c-\> :RnvimrResize<CR>


let g:nvim_tree_icons = {
    \ 'default': '',
    \ 'symlink': '',
    \ 'git': {
    \   'unstaged': "✗",
    \   'staged': "✓",
    \   'unmerged': "",
    \   'renamed': "➜",
    \   'untracked': "★",
    \   'deleted': "",
    \   'ignored': "◌"
    \   },
    \ 'folder': {
    \   'default': "",
    \   'open': "",
    \   'empty': "",
    \   'empty_open': "",
    \   'symlink': "",
    \   'symlink_open': "",
    \   }
    \ }

