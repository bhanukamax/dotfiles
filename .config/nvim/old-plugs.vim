
"Plug 'tpope/vim-sensible'
"Plug 'preservim/nerdtree'
"
"Plug 'ayu-theme/ayu-vim'
"
"Plug 'Bhanukamax/neotermman'
"Plug 'jiangmiao/auto-pairs'
"Plug 'sonph/onehalf', { 'rtp': 'vim' }
"Plug 'dbeniamine/cheat.sh-vim'
"Plug 'aserebryakov/vim-todo-lists'
"Plug 'itchyny/lightline.vim'
"Plug 'tjdevries/cyclist.vim'
"Plug 'tjdevries/gruvbuddy.nvim'
"Plug 'tjdevries/colorbuddy.vim'
"Plug 'tjdevries/gruvbuddy.nvim'
" Initialize plugin system
" Syntax stuff
"Plug 'pangloss/vim-javascript'
"Plug 'leafgarland/typescript-vim'
"Plug 'peitalin/vim-jsx-typescript'
" Telescope stuff
"Plug 'glepnir/lspsaga.nvim'
"Plug 'jose-elias-alvarez/buftabline.nvim'
    "Plug 'nvim-lua/completion-nvim'
" Minimal File browser
"Plug 'justinmk/vim-dirvish'
" Git
"Plug 'airblade/vim-gitgutter'
"Plug 'tjdevries/train.nvim'
"Plug 'ThePrimeagen/af-pluth-pluth'
"Plug 'terryma/vim-multiple-cursors'
"Plug 'phaazon/hop.nvim'
"Plug 'stevearc/vim-arduino'
"Plug 'joshdick/onedark.vim'
"
"Plug 'romainl/flattened'
"Plug 'pseewald/vim-anyfold'
"Plug 'TimUntersberger/neogit'
"Plug 'kevinhwang91/rnvimr' "Floating ranger
"Plug 'romgrk/lib.kom'
"
"
"
"
"Plug 'mildred/vim-bufmru' " Switch buffers in most recently used order
"Plug 'simeji/winresizer' " Resize windows with ctrl+q
"Plug 'vifm/vifm.vim'
"Plug 'tpope/vim-surround'
"Plug 'vim-utils/vim-man'
"Plug 'rktjmp/lush.nvim'
"Plug 'psliwka/vim-smoothie'
"
"Create new modes
"Plug 'Iron-E/nvim-libmodal'
"
"vim viki
"Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }
"Plug 'ferrine/md-img-paste.vim'
"
"Plug 'mfussenegger/nvim-dap'
