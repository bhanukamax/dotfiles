syntax on
let g:bmax_use_lsp = 1 " 1 -> lsp or 0 -> coc

let g:winresizer_start_key = '<leader>q'

" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'preservim/nerdcommenter'
Plug 'mattn/emmet-vim'

Plug 'machakann/vim-sandwich'
Plug 'rking/ag.vim' " Silver search
Plug 'tpope/vim-fugitive' " Git integrations
Plug 'sodapopcan/vim-twiggy' " Git branch managment
Plug 'APZelos/blamer.nvim' " Show git blames
Plug 'szw/vim-maximizer' " Maximize current split
Plug 'rust-lang/rust.vim'
Plug 'schickling/vim-bufonly' " Close all buffers
Plug 'tpope/vim-abolish'
Plug 'mbbill/undotree'
"
"
Plug 'neovim/nvim-lspconfig'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
"Plug 'nvim-treesitter/playground'

if g:bmax_use_lsp == 1
    Plug 'jose-elias-alvarez/nvim-lsp-ts-utils', {'branch': 'develop'}
    Plug 'hrsh7th/nvim-compe'
else
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
endif

Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-telescope/telescope-fzy-native.nvim'

Plug 'lewis6991/gitsigns.nvim'
"
" Colors
Plug 'embark-theme/vim', { 'as': 'embark' }
Plug 'voldikss/vim-floaterm'
Plug 'flazz/vim-colorschemes'
Plug 'morhetz/gruvbox'
Plug 'glepnir/oceanic-material'

" Icons
Plug 'ryanoasis/vim-devicons'
Plug 'kristijanhusak/defx-icons'
Plug 'kyazdani42/nvim-web-devicons' " for file icons

Plug 'tpope/vim-rhubarb'
Plug 'stsewd/fzf-checkout.vim'
Plug 'Shougo/defx.nvim', { 'do': ':UpdateRemotePlugins' } " Trying a new file manager
Plug 'fszymanski/fzf-quickfix', {'on': 'Quickfix'}

Plug 'ThePrimeagen/harpoon'
Plug 'ThePrimeagen/vim-be-good'

Plug 'kyazdani42/nvim-tree.lua'
Plug 'norcalli/nvim-colorizer.lua'

"
" Smooth scrolling
"Plug 'karb94/neoscroll.nvim'

Plug 'brenopacheco/vim-hydra'
Plug 'hoob3rt/lualine.nvim'

" Wiki
Plug 'vimwiki/vimwiki'



Plug 'evanleck/vim-svelte'
Plug 'prettier/vim-prettier', { 'do': 'npm install' }


call plug#end()

let g:vimwiki_list = [{'path': '~/Dropbox/vimwiki/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]


let mapleader=" "

set exrc "This does not seem to work from the ./plugin/sets.vim file

:lua require('lualine').setup {
            \ options =
            \    {
            \    theme = 'onedark'
            \    }
            \ }
lua require('gitsigns').setup()



au! BufNewFile,BufRead *.svelte set ft=html
let g:prettier#quickfix_enabled = 0
let g:prettier#autoformat_require_pragma = 0
au BufWritePre *.svelte PrettierAsync

if maparg('<C-L>', 'n') ==# ''
  nnoremap <silent> <C-L> :nohlsearch<C-R>=has('diff')?'<Bar>diffupdate':''<CR><CR><C-L>
endif





lua << EOF
require("harpoon").setup({
    global_settings = {
        save_on_toggle = true,
        save_on_change = true,
    },
    menu = {
        width = 120,
        height = 15,
    }
})



EOF


"nnoremap <leader>bf <Plug>(Prettier)
vnoremap <leader>bf :PrettierPartial<CR>
nmap <unique> <Leader>bf <Plug>(Prettier)


" asdjfka sfasdfjadsf Dolor ab totam commodi iure corrupti similique. Voluptatem iure vero nobis accusamus non cumque! Dignissimos qui ipsum vitae molestias cum cumque! Possimus repellendus doloribus a ipsa laborum? Ad facilis quod.

" lua vim.lsp.set_log_level("debug")
" lua print(vim.lsp.get_log_path())
"lua require("buftabline").setup {}
"
":set listchars=trail:𝁢,, eol:↲,nbsp:☠,conceal:┊,tab:» ,extends:…,precedes:…
lua <<EOF
require'bmax.treesitter'.use_tree_sitter()
EOF


