local M = {}


M.nav_file = function(file_number)
    require("harpoon.ui").nav_file(file_number)
    print(" (" .. tostring(file_number) .. ") " .. vim.fn.bufname())

end

return M
