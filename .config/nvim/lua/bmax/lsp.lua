
local M = {}



M.setup = function(file_number)

    --vim.cmd('colorscheme gruvbox')
    require'lspconfig'.tsserver.setup{}

    require'lspconfig'.clangd.setup{}


    vim.o.completeopt = "menuone,noselect"

    require'compe'.setup {
        enabled = true;
        autocomplete = true;
        debug = false;
        min_length = 1;
        preselect = 'enable';
        throttle_time = 80;
        source_timeout = 200;
        incomplete_delay = 400;
        max_abbr_width = 100;
        max_kind_width = 100;
        max_menu_width = 100;
        documentation = true;

        source = {
            path = true;
            buffer = true;
            calc = true;
            nvim_lsp = true;
            nvim_lua = true;
            vsnip = true;
        };
    }
    M.setupLSPUtils()
end


M.setupLSPUtils = function()
    local nvim_lsp = require("lspconfig")

    nvim_lsp.tsserver.setup {
        on_attach = function(client, bufnr)
            local ts_utils = require("nvim-lsp-ts-utils")

            -- defaults
            ts_utils.setup {
                disable_commands = false,
                debug = false,
                enable_import_on_completion = false,
                import_on_completion_timeout = 5000,

                -- eslint
                eslint_bin = "eslint",
                eslint_args = {"-f", "json", "--stdin", "--stdin-filename", "$FILENAME"},
                eslint_enable_disable_comments = true,

                -- experimental settings!
                -- eslint diagnostics
                eslint_enable_diagnostics = false,
                eslint_diagnostics_debounce = 250,

                -- formatting
                enable_formatting = false,
                formatter = "prettier",
                formatter_args = {"--stdin-filepath", "$FILENAME"},
                format_on_save = false,
                no_save_after_format = false,
                -- parentheses completion
                complete_parens = false,
                signature_help_in_parens = false,

                -- update imports on file move
                update_imports_on_move = false,
                require_confirmation_on_move = false,
                watch_dir = "/src",
            }

            -- required to enable ESLint code actions and formatting
            ts_utils.setup_client(client)

            -- no default maps, so you may want to define some here
            vim.api.nvim_buf_set_keymap(bufnr, "n", "gs", ":TSLspOrganize<CR>", {silent = true})
            vim.api.nvim_buf_set_keymap(bufnr, "n", "qq", ":TSLspFixCurrent<CR>", {silent = true})
            vim.api.nvim_buf_set_keymap(bufnr, "n", "gr", ":TSLspRenameFile<CR>", {silent = true})
            vim.api.nvim_buf_set_keymap(bufnr, "n", "gi", ":TSLspImportAll<CR>", {silent = true})
        end
    }

svelteserver = {
  on_attach = function(client)
    mappings.lsp_mappings()

    client.server_capabilities.completionProvider.triggerCharacters = {
      ".", '"', "'", "`", "/", "@", "*",
      "#", "$", "+", "^", "(", "[", "-", ":"
    }
  end,
  on_init = custom_on_init,
  handlers = {
    ["textDocument/publishDiagnostics"] = is_using_eslint,
  },
  filetypes = { 'html', 'svelte' },
  settings = {
    svelte = {
      plugin = {
        -- some settings
      },
    },
  },
}

end


return M


