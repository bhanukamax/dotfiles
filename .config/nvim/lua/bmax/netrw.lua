local M = {}
M.p = function(something)
    print(vim.inspect(something))
end

-- Open netrw and keep the cursor on the current file
M.open_netrw = function()
    file_name = vim.fn.expand('%:t')
    vim.cmd(":Ex")
    vim.fn.search(file_name)
end


M.Boop = function(...)
  local string = ""
  for i, v in ipairs({ ... }) do
    string = string .. tostring(v) .. " - "
  end
  print(string)
end


M.WikiPreview = function()

    --local ft  = vim.fn.expand('&ft')
    --local ft  = vim.fn.expand('&ft') vim.api.nvim_buf_get_option(0, "filetype")
    --local ft = vim.bo.filetype
    local ft = vim.api.nvim_buf_get_option(0, "filetype")
    if ft == 'vimwiki' then


    end
    print(ft)

end

return M
