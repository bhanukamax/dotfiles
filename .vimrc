" THIS IS NOT THE ACTUAL VIMRC, PLEASE CHECK THE ONE AT ./config/nvim/init.vim
syntax on
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
call plug#begin('~/.vim/plugged')

Plug 'neoclide/coc.nvim'

call plug#end()

let mapleader=" "

set so=4

fun! OpenNetrw()
    let g:file_name = expand('%:t')
    :Ex
    :call search(g:file_name)
endfun

inoremap jk <Esc>

nmap <c-s> :echo "WHAT"<CR>
nnoremap <leader>ve :e $MYVIMRC<CR>

" NETRW
nnoremap - :call OpenNetrw()<CR>

let g:netrw_banner = 0

augroup netrw_mapping
    autocmd!
    autocmd filetype netrw call NetrwMapping()
augroup END

function! NetrwMapping()
    map <buffer> h -
    map <buffer> l <CR>
endfunction

inoremap <c-s> <Esc>:w<CR>l
nnoremap <c-s> :w<CR>


" COC
vmap <leader>ac  <Plug>(coc-codeaction-selected)
xmap <leader>ac  <Plug>(coc-codeaction-selected)
nmap <leader>ac  <Plug>(coc-codeaction-selected)

