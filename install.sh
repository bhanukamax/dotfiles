#!/bin/bash
# find .config -regex ".*\.vim$\|.*\.lua$\|.*\.ron$\|.*\.yml$\|.*\.py$\|.*\.sh$\|.*\.conf$"
# find . -regex ".*\.vim$\|.*\.ron$"
mkdir -p ~/.config/nvim/plugin
mkdir -p ~/.config/nvim/lua/bmax
mkdir -p ~/.config/awesome/awesome.battery-widget
mkdir -p ~/.config/alacritty
mkdir -p ~/.config/gitui
mkdir -p ~/.config/kitty
mkdir -p ~/.config/qtile
mkdir -p ~/.config/sway
mkdir -p ~/.config/ranger
mkdir -p ~/.config/i3status
mkdir -p ~/.i3
mkdir -p ~/test/node-stuff/

# copy .config files
for f in $( find .config -regex ".*\.vim$\|.*\.lua$\|.*\.ron$\|.*\.yml$\|.*\.py$\|.*\.sh$\|.*\.conf$\|.*\.json$" ); do
  rm -rf ~/$f
  ln -s ~/$(basename $PWD)/$f ~/$f
done

rm -rf ~/.config/sway/config
ln -s ~/$(basename $PWD)/.config/sway/config  ~/.config/sway/config

# vim for termux (on android)
rm -rf ~/.vimrc
ln -s ~/$(basename $PWD)/.vimrc  ~/.vimrc


rm -rf ~/.config/i3status/config
ln -s ~/$(basename $PWD)/.config/i3status/config  ~/.config/i3status/config

# copy i3 config files
for f in $(ls .i3); do
  rm -rf ~/.i3/$f
  ln -s ~/$(basename $PWD)/.i3/$f ~/.i3/$f
done

# copy root files
for file in $( cat files ); do
  #echo $file
  rm -rf ~/$file
  ln -s "$(pwd)/$file" ~/$file
done
